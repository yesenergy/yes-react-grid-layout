// @flow
/* eslint-env jest */

/*:: declare function describe(name: string, fn: Function): void; */
/*:: declare function it(name: string, fn: Function): void; */
/*:: declare var expect: {
  (any): any,
  objectContaining(params: any): any
}; */

import {
  buildCollisionGrid,
  findLargestAvailableRectangle
} from "../../lib/utils.js";

describe("Shrink to fit functionality works", () => {
  it("Handles simple 1 square overlap with 1 other widget", () => {
    const layout = [
      {
        x: 0,
        y: 0,
        w: 2,
        h: 2,
        i: 1
      }
    ];
    const collisionDetector = {
      x: 1,
      y: 1,
      w: 2,
      h: 2,
      i: 2
    };
    const expectedDetectorGrid = {
      "1": {
        "1": false,
        "2": true
      },
      "2": {
        "1": true,
        "2": true
      }
    };
    const overlap = buildCollisionGrid(layout, collisionDetector);
    expect(overlap).toEqual(expectedDetectorGrid);
  });
  it("Handles multiple varying square overlaps with 3 other widgets", () => {
    const layout = [
      {
        x: 0,
        y: 0,
        w: 2,
        h: 2,
        i: 1
      },
      {
        x: 3,
        y: 3,
        h: 5,
        w: 5,
        i: 3
      },
      {
        x: 2,
        y: 3,
        w: 1,
        h: 1,
        i: 4
      }
    ];
    const collisionDetector = {
      x: 1,
      y: 1,
      w: 4,
      h: 4,
      i: 2
    };
    const expectedDetectorGrid = {
      '1': {
        '1': false,
        '2': true,
        '3': true,
        '4': true
      },
      '2': {
        '1': true,
        '2': true,
        '3': false,
        '4': true
      },
      '3': {
        '1': true,
        '2': true,
        '3': false,
        '4': false
      },
      '4': {
        '1': true,
        '2': true,
        '3': false,
        '4': false
      }
    };
    const overlap = buildCollisionGrid(layout, collisionDetector);
    expect(overlap).toEqual(expectedDetectorGrid);
  });
});

describe("Handle cumulative width of a series of open spaces in a row", () => {
  it("Handles creating a matrix of running widths of the areas not covered by collision", () => {
    const layout = [
      {
        x: 0,
        y: 0,
        w: 2,
        h: 2,
        i: 1
      },
      {
        x: 3,
        y: 3,
        h: 5,
        w: 5,
        i: 3
      },
      {
        x: 2,
        y: 3,
        w: 1,
        h: 1,
        i: 4
      }
    ];
    const collisionDetector = {
      x: 1,
      y: 1,
      w: 4,
      h: 4,
      i: 2
    };
    const expectedMaxArea = { area: 6, y: 1, x: 2, h: 2, w: 3 };
    const runningHeightsOnRow = findLargestAvailableRectangle(
      layout,
      collisionDetector
    );
    expect(runningHeightsOnRow).toEqual(expectedMaxArea);
  });
});
