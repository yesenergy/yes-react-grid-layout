// @flow
import React from "react";
import PropTypes from "prop-types";
import isEqual from "lodash.isequal";
import classNames from "classnames";
import debounce from "lodash/debounce";
import {
  autoBindHandlers,
  bottom,
  childrenEqual,
  cloneLayoutItem,
  getLayoutItem,
  synchronizeLayoutWithChildren,
  validateLayout,
  getFirstCollision,
  noop,
  findLargestAvailableRectangle,
  calcColWidth,
  moveElement,
  compact
} from "./utils";
import GridItem from "./GridItem";
import type {
  ChildrenArray as ReactChildrenArray,
  Element as ReactElement
} from "react";

// Types
import type {
  EventCallback,
  CompactType,
  GridResizeEvent,
  GridDragEvent,
  DragOverEvent,
  Layout,
  DroppingPosition,
  LayoutItem
} from "./utils";

type State = {
  activeDrag: ?LayoutItem,
  layout: Layout,
  mounted: boolean,
  oldDragItem: ?LayoutItem,
  oldLayout: ?Layout,
  oldResizeItem: ?LayoutItem,
  droppingDOMNode: ?ReactElement<any>,
  droppingPosition?: DroppingPosition,
  dragSuggestion: boolean,
  // Mirrored props
  children: ReactChildrenArray<ReactElement<any>>,
  compactType?: CompactType,
  propsLayout?: Layout
};

export type Props = {
  className: string,
  style: Object,
  width: number,
  autoSize: boolean,
  cols: number,
  draggableCancel: string,
  draggableHandle: string,
  verticalCompact: boolean,
  compactType: CompactType,
  layout: Layout,
  margin: [number, number],
  containerPadding: [number, number] | null,
  rowHeight: number,
  maxRows: number,
  isDraggable: boolean,
  isResizable: boolean,
  isDroppable: boolean,
  preventCollision: boolean,
  useCSSTransforms: boolean,
  transformScale: number,
  droppingItem: $Shape<LayoutItem>,
  fixedNxN: boolean,
  currentDefaultW: number,
  currentDefaultH: number,

  // Callbacks
  onLayoutChange: Layout => void,
  onDrag: EventCallback,
  onDragStart: EventCallback,
  onDragStop: EventCallback,
  onDragOver: EventCallback,
  onResize: EventCallback,
  onResizeStart: EventCallback,
  onResizeStop: EventCallback,
  onDrop: (itemPosition: {
    x: number,
    y: number,
    w: number,
    h: number,
    e: Event
  }) => void,
  children: ReactChildrenArray<ReactElement<any>>
};
// End Types

const compactType = (props: Props): CompactType => {
  const { verticalCompact, compactType } = props || {};

  return verticalCompact === false ? null : compactType;
};

const layoutClassName = "react-grid-layout";
let isFirefox = false;
// Try...catch will protect from navigator not existing (e.g. node) or a bad implementation of navigator
try {
  isFirefox = /firefox/i.test(navigator.userAgent);
} catch (e) {
  /* Ignore */
}

/**
 * A reactive, fluid grid layout with draggable, resizable components.
 */

export default class ReactGridLayout extends React.Component<Props, State> {
  // TODO publish internal ReactClass displayName transform
  static displayName = "ReactGridLayout";

  static propTypes = {
    //
    // Basic props
    //
    className: PropTypes.string,
    style: PropTypes.object,

    // This can be set explicitly. If it is not set, it will automatically
    // be set to the container width. Note that resizes will *not* cause this to adjust.
    // If you need that behavior, use WidthProvider.
    width: PropTypes.number,
    // If true, the container height swells and contracts to fit contents
    autoSize: PropTypes.bool,
    // # of cols.
    cols: PropTypes.number,

    // A selector that will not be draggable.
    draggableCancel: PropTypes.string,
    // A selector for the draggable handler
    draggableHandle: PropTypes.string,

    // Deprecated
    verticalCompact: function(props: Props) {
      if (
        props.verticalCompact === false &&
        process.env.NODE_ENV !== "production"
      ) {
        console.warn(
          // eslint-disable-line no-console
          "`verticalCompact` on <ReactGridLayout> is deprecated and will be removed soon. " +
            'Use `compactType`: "horizontal" | "vertical" | null.'
        );
      }
    },
    // Choose vertical or hotizontal compaction
    compactType: PropTypes.oneOf(["vertical", "horizontal"]),

    // layout is an array of object with the format:
    // {x: Number, y: Number, w: Number, h: Number, i: String}
    layout: function(props: Props) {
      var layout = props.layout;
      // I hope you're setting the data-grid property on the grid items
      if (layout === undefined) return;
      validateLayout(layout, "layout");
    },

    //
    // Grid Dimensions
    //

    // Margin between items [x, y] in px
    margin: PropTypes.arrayOf(PropTypes.number),
    // Padding inside the container [x, y] in px
    containerPadding: PropTypes.arrayOf(PropTypes.number),
    // Rows have a static height, but you can change this based on breakpoints if you like
    rowHeight: PropTypes.number,
    // Default Infinity, but you can specify a max here if you like.
    // Note that this isn't fully fleshed out and won't error if you specify a layout that
    // extends beyond the row capacity. It will, however, not allow users to drag/resize
    // an item past the barrier. They can push items beyond the barrier, though.
    // Intentionally not documented for this reason.
    maxRows: PropTypes.number,

    //
    // Flags
    //
    isDraggable: PropTypes.bool,
    isResizable: PropTypes.bool,
    // If true, grid items won't change position when being dragged over.
    preventCollision: PropTypes.bool,
    // Use CSS transforms instead of top/left
    useCSSTransforms: PropTypes.bool,
    // parent layout transform scale
    transformScale: PropTypes.number,
    // If true, an external element can trigger onDrop callback with a specific grid position as a parameter
    isDroppable: PropTypes.bool,
    fixedNxN: PropTypes.bool,

    //
    // Callbacks
    //

    // Callback so you can save the layout. Calls after each drag & resize stops.
    onLayoutChange: PropTypes.func,

    // Calls when drag starts. Callback is of the signature (layout, oldItem, newItem, placeholder, e, ?node).
    // All callbacks below have the same signature. 'start' and 'stop' callbacks omit the 'placeholder'.
    onDragStart: PropTypes.func,
    // Calls on each drag movement.
    onDrag: PropTypes.func,
    // Calls when dragged into movement area
    onDragOver: PropTypes.func,
    // Calls when drag is complete.
    onDragStop: PropTypes.func,
    //Calls when resize starts.
    onResizeStart: PropTypes.func,
    // Calls when resize movement happens.
    onResize: PropTypes.func,
    // Calls when resize is complete.
    onResizeStop: PropTypes.func,
    // Calls when some element is dropped.
    onDrop: PropTypes.func,

    //
    // Other validations
    //

    droppingItem: PropTypes.shape({
      i: PropTypes.string.isRequired,
      w: PropTypes.number.isRequired,
      h: PropTypes.number.isRequired
    }),

    // Children must not have duplicate keys.
    children: function(props: Props, propName: string) {
      var children = props[propName];

      // Check children keys for duplicates. Throw if found.
      var keys = {};
      React.Children.forEach(children, function(child) {
        if (keys[child.key]) {
          throw new Error(
            'Duplicate child key "' +
              child.key +
              '" found! This will cause problems in ReactGridLayout.'
          );
        }
        keys[child.key] = true;
      });
    },
    //flag to turn on and off the feature that resizes a GridItem when a collision occurs
    isShrinkable: PropTypes.bool
  };

  static defaultProps = {
    autoSize: true,
    cols: 12,
    className: "",
    style: {},
    draggableHandle: "",
    draggableCancel: "",
    containerPadding: null,
    rowHeight: 150,
    maxRows: Infinity, // infinite vertical growth
    layout: [],
    margin: [10, 10],
    isDraggable: true,
    isResizable: true,
    isDroppable: false,
    useCSSTransforms: true,
    transformScale: 1,
    verticalCompact: true,
    compactType: "vertical",
    preventCollision: false,
    droppingItem: {
      i: "__dropping-elem__",
      w: 1,
      h: 1
    },
    fixedNxN: false,
    currentDefaultW: 1,
    currentDefaultH: 1,
    isShrinkable: true,
    onLayoutChange: noop,
    onDragStart: noop,
    onDragOver: noop,
    onDrag: noop,
    onDragStop: noop,
    onResizeStart: noop,
    onResize: noop,
    onResizeStop: noop,
    onDrop: noop
  };

  state: State = {
    activeDrag: null,
    layout: synchronizeLayoutWithChildren(
      this.props.layout,
      this.props.children,
      this.props.cols,
      // Legacy support for verticalCompact: false
      compactType(this.props)
    ),
    mounted: false,
    oldDragItem: null,
    oldLayout: null,
    oldResizeItem: null,
    droppingDOMNode: null,
    dragSuggestion: false,
    children: []
  };

  dragEnterCounter = 0;

  constructor(props: Props, context: any): void {
    super(props, context);
    autoBindHandlers(this, [
      "onDragStart",
      "onDrag",
      "onDragStop",
      "onResizeStart",
      "onResize",
      "onResizeStop",
      "onDragLeave",
      "onDragOver"
    ]);
  }

  componentDidMount() {
    this.setState({ mounted: true });
    this.gridSize = React.createRef();
    window.addEventListener("dragend", this.offscreenRemovePlaceholder, false);

    // Possibly call back with layout on mount. This should be done after correcting the layout width
    // to ensure we don't rerender with the wrong width.
    this.props.onLayoutChange(this.state.layout);
  }

  componentWillUnmount() {
    // Remove on component unmount
    window.removeEventListener(
      "dragend",
      this.offscreenRemovePlaceholder,
      false
    );
  }

  static getDerivedStateFromProps(nextProps: Props, prevState: State) {
    let newLayoutBase;

    if (prevState.activeDrag) {
      return null;
    }

    // Legacy support for compactType
    // Allow parent to set layout directly.
    if (
      !isEqual(nextProps.layout, prevState.propsLayout) ||
      nextProps.compactType !== prevState.compactType
    ) {
      newLayoutBase = nextProps.layout;
    } else if (!childrenEqual(nextProps.children, prevState.children)) {
      // If children change, also regenerate the layout. Use our state
      // as the base in case because it may be more up to date than
      // what is in props.
      newLayoutBase = prevState.layout;
    }

    // We need to regenerate the layout.
    if (newLayoutBase) {
      const newLayout = synchronizeLayoutWithChildren(
        newLayoutBase,
        nextProps.children,
        nextProps.cols,
        compactType(nextProps)
      );

      return {
        layout: newLayout,
        // We need to save these props to state for using
        // getDerivedStateFromProps instead of componentDidMount (in which we would get extra rerender)
        compactType: nextProps.compactType,
        children: nextProps.children,
        propsLayout: nextProps.layout
      };
    }

    return null;
  }

  /**
   * Calculates a pixel value for the container.
   * @return {String} Container height in pixels.
   */
  containerHeight() {
    // PATCH NOTE: Only place the fixedNxN is used. Otherwise we can't have maxheight
    if (this.props.fixedNxN) return "100%";
    if (!this.props.autoSize) return;
    const nbRow = bottom(this.state.layout);
    const containerPaddingY = this.props.containerPadding
      ? this.props.containerPadding[1]
      : this.props.margin[1];
    return (
      nbRow * this.props.rowHeight +
      (nbRow - 1) * this.props.margin[1] +
      containerPaddingY * 2 +
      "px"
    );
  }

  /**
   * When dragging starts
   * @param {String} i Id of the child
   * @param {Number} x X position of the move
   * @param {Number} y Y position of the move
   * @param {Event} e The mousedown event
   * @param {Element} node The current dragging DOM element
   */
  onDragStart(i: string, x: number, y: number, { e, node }: GridDragEvent) {
    const { layout } = this.state;
    const l = getLayoutItem(layout, i);
    if (!l) return;

    this.setState({
      oldDragItem: cloneLayoutItem(l),
      oldLayout: this.state.layout
    });

    return this.props.onDragStart(layout, l, x, y, e, node);
  }

  /**
   * Each drag movement create a new dragelement and move the element to the dragged location
   * @param {String} i Id of the child
   * @param {Number} x X position of the move
   * @param {Number} y Y position of the move
   * @param {Event} e The mousedown event
   * @param {Element} node The current dragging DOM element
   */
  onDrag(i: string, x: number, y: number, { e, node }: GridDragEvent) {
    console;
    const { layout, oldDragItem } = this.state;
    var l = getLayoutItem(layout, i);
    const { cols } = this.props;
    if (!l) return;
    // Create placeholder (display only)
    var placeholder = {
      w: l.w,
      h: l.h,
      x: l.x,
      y: l.y,
      placeholder: true,
      i: i
    };

    // PATCH NOTE: USES TOP/LEFT MOST X/Y OF DRAGGED GRID ITEM INSTEAD OF EXISTING LAYOUT ITEM
    var collisionDetector = {
      w: l.w,
      h: l.h,
      x: x,
      y: y,
      placeholder: true,
      i: i
    };

    if (this.props.isShrinkable) {
      // checkOffPage Mutates the collision detector to makes sure we never go off page
      const { offPage, cappedCD } = this.checkOffPage(collisionDetector, x, y);
      const hasCollision = getFirstCollision(layout, cappedCD);

      if (hasCollision && this.props.isShrinkable) {
        this.shrinkToFit(layout, cappedCD);
      } else if (offPage) {
        this.setState({
          dragSuggestion: true
        });

        this.props.onDrag(layout, oldDragItem, l, placeholder, e, node);

        this.setState({
          activeDrag: cappedCD
        });
      } else {
        this.setState({
          dragSuggestion: false
        });
        this.props.onDrag(layout, oldDragItem, l, placeholder, e, node);

        this.setState({
          activeDrag: cappedCD
        });
      }
    } else {
      // Move the element to the dragged location.
      const isUserAction = true;
      const newLayout = moveElement(
        layout,
        l,
        x,
        y,
        isUserAction,
        this.props.preventCollision,
        compactType(this.props),
        cols
      );

      this.props.onDrag(newLayout, oldDragItem, l, placeholder, e, node);

      this.setState({
        layout: compact(newLayout, compactType(this.props), cols),
        activeDrag: placeholder
      });
    }
  }

  checkOffPage = (collisionDetector, x, y) => {
    const { maxRows, cols } = this.props;
    const cappedCD = { ...collisionDetector };

    let offPage = false;
    // Left edge collision
    if (x < 0) {
      // Checks that we never go below 1 w/h
      const onPageX = x + cappedCD.w < 1 ? -1 * cappedCD.w + 1 : x;
      cappedCD.w = cappedCD.w + onPageX;
      cappedCD.x = 0;
      offPage = true;
    }
    // Right edge collision
    if (x + cappedCD.w > cols) {
      // Checks that we never go below 1 w/h
      const onPageX = x > cols - 1 ? cols - 1 : x;
      cappedCD.w = cappedCD.w - cappedCD.w - onPageX + cols;
      cappedCD.x = onPageX;
      offPage = true;
    }
    // Top Edge collision
    if (y < 0) {
      // Checks that we never go below 1 w/h
      const onPageY = y + cappedCD.h < 1 ? -1 * cappedCD.h + 1 : y;
      cappedCD.h = cappedCD.h + onPageY;
      cappedCD.y = 0;
      offPage = true;
    }
    // Bottom edge collision
    if (y + cappedCD.h > maxRows) {
      // Checks that we never go below 1 w/h
      const onPageY = y > maxRows - 1 ? maxRows - 1 : y;
      cappedCD.h = cappedCD.h - cappedCD.h - onPageY + maxRows;
      cappedCD.y = onPageY;
      offPage = true;
    }
    return { offPage, cappedCD };
  };

  /*
    New functionality to "snap" a grid item to largest available spac
    findLargestAvailableRectangle will take the currently dragged item
    and determine the largest available area behind it and send back
    a resized Grid Item that will render a preview placeholder of the dropped item
  */
  shrinkToFit = (layout, collisionDetector) => {
    const maxAreaSpaceToMove = findLargestAvailableRectangle(
      layout,
      collisionDetector
    );
    maxAreaSpaceToMove.i = collisionDetector.i;
    if (maxAreaSpaceToMove.h > 0 && maxAreaSpaceToMove.w > 0) {
      this.setState({
        activeDrag: maxAreaSpaceToMove,
        dragSuggestion: true
      });
    }
  };

  /**
   * When dragging stops, figure out which position the element is closest to and update its x and y.
   * @param  {String} i Index of the child.
   * @param {Number} x X position of the move
   * @param {Number} y Y position of the move
   * @param {Event} e The mousedown event
   * @param {Element} node The current dragging DOM element
   */
  onDragStop(i: string, x: number, y: number, { e, node }: GridDragEvent) {
    if (!this.state.activeDrag) return;

    const { oldDragItem, activeDrag } = this.state;
    const { layout } = this.state;
    let l;
    let freshLayout = [...layout];
    // PATCH NOTE: set dims to placeholder (activeDrag) instead of original size
    freshLayout = layout.map(item => {
      if (item.i === activeDrag.i) {
        const newItem = {
          ...item,
          w: activeDrag.w,
          h: activeDrag.h,
          x: activeDrag.x,
          y: activeDrag.y,
          placeholder: false
        };
        return newItem;
      }
      return item;
    });

    // Set state
    this.props.onDragStop(freshLayout, oldDragItem, l, null, e, node);
    this.setState({
      activeDrag: null,
      layout: freshLayout,
      oldDragItem: null,
      oldLayout: null
    });
    this.props.onLayoutChange(freshLayout);
  }

  onResizeStart(i: string, w: number, h: number, { e, node }: GridResizeEvent) {
    const { layout } = this.state;
    var l = getLayoutItem(layout, i);
    if (!l) return;

    this.setState({
      oldResizeItem: cloneLayoutItem(l),
      oldLayout: this.state.layout
    });

    this.props.onResizeStart(layout, l, l, null, e, node);
  }

  onResize(i: string, w: number, h: number, { e, node }: GridResizeEvent) {
    const { layout, oldResizeItem } = this.state;
    const l: ?LayoutItem = getLayoutItem(layout, i);
    if (!l) return;

    const hasCollision = getFirstCollision(layout, { ...l, w, h });
    let placeholder;
    // If we're colliding, we need adjust the placeholder.
    if (hasCollision && this.props.isShrinkable) {
      this.shrinkToFit(layout, { ...l, w, h });
    } else {
      placeholder = {
        w: w,
        h: h,
        x: l.x,
        y: l.y,
        static: true,
        i: i
      };
      this.props.onResize(layout, oldResizeItem, l, placeholder, e, node);
      this.setState({
        activeDrag: placeholder
      });
    }
  }

  onResizeStop(i: string, w: number, h: number, { e, node }: GridResizeEvent) {
    if (!this.state.activeDrag) return;
    const { layout, activeDrag } = this.state;
    var l = getLayoutItem(layout, i);

    // PATCH NOTE: set dims to placeholder instead of original size
    const newLayout = layout.map(item => {
      if (item.i === activeDrag.i) {
        const newItem = {
          ...item,
          w: activeDrag.w,
          h: activeDrag.h,
          x: activeDrag.x,
          y: activeDrag.y,
          placeholder: false
        };
        return newItem;
      }
      return item;
    });

    const { oldLayout } = this.state;
    this.props.onResizeStop(newLayout, oldLayout, l, null, e, node);

    this.setState({
      activeDrag: null,
      layout: newLayout,
      oldResizeItem: null,
      oldLayout: null
    });

    this.props.onLayoutChange(newLayout);
  }

  /**
   * Create a placeholder object.
   * @return {Element} Placeholder div.
   */
  placeholder(): ?ReactElement<any> {
    const { activeDrag } = this.state;

    const {
      width,
      cols,
      margin,
      containerPadding,
      rowHeight,
      maxRows,
      useCSSTransforms,
      transformScale
    } = this.props;

    // {...this.state.activeDrag} is pretty slow, actually
    return (
      <GridItem
        w={activeDrag.w}
        h={activeDrag.h}
        x={activeDrag.x}
        y={activeDrag.y}
        i={activeDrag.i}
        className="react-grid-placeholder"
        containerWidth={width}
        cols={cols}
        margin={margin}
        containerPadding={containerPadding || margin}
        maxRows={maxRows}
        rowHeight={rowHeight}
        isDraggable={false}
        isResizable={false}
        useCSSTransforms={useCSSTransforms}
        transformScale={transformScale}
      >
        <div />
      </GridItem>
    );
  }

  /**
   * Given a grid item, set its style attributes & surround in a <Draggable>.
   * @param  {Element} child React element.
   * @return {Element}       Element wrapped in draggable and properly placed.
   */
  processGridItem(
    child: ReactElement<any>,
    isDroppingItem?: boolean
  ): ?ReactElement<any> {
    if (!child || !child.key) return;
    const l = getLayoutItem(this.state.layout, String(child.key));
    if (!l) return null;
    const {
      width,
      cols,
      margin,
      containerPadding,
      maxRows,
      rowHeight,
      isDraggable,
      isResizable,
      useCSSTransforms,
      transformScale,
      draggableCancel,
      draggableHandle
    } = this.props;
    const { mounted, droppingPosition, layout } = this.state;

    // Determine user manipulations possible.
    // If an item is static, it can't be manipulated by default.
    // Any properties defined directly on the grid item will take precedence.
    const draggable =
      typeof l.isDraggable === "boolean"
        ? l.isDraggable
        : !l.static && isDraggable;

    const resizable =
      typeof l.isResizable === "boolean"
        ? l.isResizable
        : !l.static && isResizable;

    return (
      <GridItem
        containerWidth={width}
        cols={cols}
        margin={margin}
        layout={layout}
        containerPadding={containerPadding || margin}
        maxRows={maxRows}
        rowHeight={rowHeight}
        cancel={draggableCancel}
        handle={draggableHandle}
        onDragStop={this.onDragStop}
        onDragStart={this.onDragStart}
        onDrag={this.onDrag}
        onResizeStart={this.onResizeStart}
        onResize={this.onResize}
        onResizeStop={this.onResizeStop}
        isDraggable={draggable}
        isResizable={resizable}
        useCSSTransforms={useCSSTransforms && mounted}
        usePercentages={!mounted}
        transformScale={transformScale}
        w={l.w}
        h={l.h}
        x={l.x}
        y={l.y}
        i={l.i}
        minH={l.minH}
        minW={l.minW}
        maxH={l.maxH}
        maxW={l.maxW}
        static={l.static}
        droppingPosition={isDroppingItem ? droppingPosition : undefined}
      >
        {child}
      </GridItem>
    );
  }

  onDragOver = (e: DragOverEvent) => {
    const {
      droppingItem,
      cols,
      maxRows,
      rowHeight,
      margin,
      containerPadding,
      width
    } = this.props;
    if (e.target.classList && e.target.classList.contains("QSNotDraggable")) {
      return;
    }
    const { layout } = this.state;
    const { pageX, pageY } = e;
    const colWidth = calcColWidth(
      margin,
      containerPadding || margin,
      width,
      cols
    );
    // This means it's off page
    if (pageX === 0 && pageY === 0) return;
    if (!this.gridSize.current) return;

    // Set the mouse to the center of the dragged widget
    const rect = this.gridSize.current.getBoundingClientRect();
    const xOffset = (colWidth * this.props.currentDefaultW) / 2;
    const yOffset = (rowHeight * this.props.currentDefaultH) / 2;
    const rectX = rect.x ? rect.x : rect.left;
    const rectY = rect.y ? rect.y : rect.top;
    const droppingPosition = {
      x: pageX - rectX - xOffset,
      y: pageY - rectY - yOffset,
      e
    };
    if (!this.state.droppingDOMNode) {
      this.props.onDragOver();
      // PATCH NOTE: This actually adds the item to the layout
      // Before it only showed a placeholder but didn't render the item
      // We set x, y to the bottom right corner to add item as a
      // offgrid hidden element until onDrop fires
      this.setState({
        droppingDOMNode: <div key={droppingItem.i} />,
        droppingPosition,
        layout: [
          ...layout,
          {
            i: droppingItem.i,
            w: this.props.currentDefaultW,
            h: this.props.currentDefaultH,
            x: cols,
            y: maxRows,
            static: false,
            isDraggable: true
          }
        ]
      });
    } else if (this.state.droppingPosition) {
      this.debouncedOnDragOver(droppingPosition, this);
    }
    e.stopPropagation();
    e.preventDefault();
  };

  debouncedOnDragOver = debounce((droppingPosition, that) => {
    const shouldUpdatePosition =
      !droppingPosition ||
      !that.state.droppingPosition ||
      that.state.droppingPosition.x != droppingPosition.x ||
      that.state.droppingPosition.y != droppingPosition.y;
    shouldUpdatePosition && that.setState({ droppingPosition });
  }, 7);

  offscreenRemovePlaceholder = () => {
    const {
      activeDrag,
      droppingDOMNode,
      droppingPosition,
      dragEnterCounter
    } = this.state;
    // This covers the case where the user has dragged off the page
    // But still has a placeholder on the page. It saves the spot
    // And if the user lets go of the mouse will drop in the last location
    if (activeDrag && !dragEnterCounter) {
      this.onDrop();
    } else if (activeDrag || droppingDOMNode || droppingPosition) {
      // If we have anything related to a placeholder, remove it.
      // This most likely means we missed a cursor event
      this.setState({
        droppingDOMNode: null,
        activeDrag: null,
        droppingPosition: undefined
      });
    }
  };

  removeDroppingPlaceholder = () => {
    const { layout, activeDrag } = this.state;

    // PATCH NOTE: this creates a real grid item in the layout and sets its position
    const layoutNoPlaceholder = layout.map(l => {
      if (l.i === "__dropping-elem__") {
        const newL = {
          ...l,
          x: activeDrag.x,
          y: activeDrag.y,
          h: activeDrag.h,
          w: activeDrag.w,
          placeholder: false
        };
        return newL;
      }
      return l;
    });
    this.setState({
      layout: layoutNoPlaceholder,
      droppingDOMNode: null,
      activeDrag: null,
      droppingPosition: undefined
    });
    return layoutNoPlaceholder.i;
  };

  onDragLeave = () => {
    this.dragEnterCounter--;
    // Drag listener
    window.addEventListener("drag", this.onDragOver, false);
  };

  onDragEnter = () => {
    this.dragEnterCounter++;
    window.removeEventListener("drag", this.onDragOver, false);
  };

  onDrop = (e: Event) => {
    const { activeDrag } = this.state;
    if (!activeDrag) return;
    const { x, y, w, h } = activeDrag;

    // reset dragEnter counter on drop
    this.dragEnterCounter = 0;
    // PATCH NOTE: Added to actually insert the item, remove the activeDrag placeholder
    this.removeDroppingPlaceholder();

    this.props.onDrop({ x, y, w, h, e });
  };

  render() {
    const { className, style, isDroppable } = this.props;

    const mergedClassName = classNames(layoutClassName, className);
    const mergedStyle = {
      height: this.containerHeight(),
      ...style
    };

    return (
      <div
        ref={this.gridSize}
        className={mergedClassName}
        style={mergedStyle}
        onDrop={isDroppable ? this.onDrop : noop}
        onDragLeave={isDroppable ? this.onDragLeave : noop}
        onDragEnter={isDroppable ? this.onDragEnter : noop}
        onDragOver={isDroppable ? this.onDragOver : noop}
      >
        {React.Children.map(this.props.children, child =>
          this.processGridItem(child)
        )}
        {isDroppable &&
          this.state.droppingDOMNode &&
          this.processGridItem(this.state.droppingDOMNode, true)}
        {this.state.activeDrag && this.placeholder()}
      </div>
    );
  }
}
