"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _lodash = _interopRequireDefault(require("lodash.isequal"));

var _classnames = _interopRequireDefault(require("classnames"));

var _debounce = _interopRequireDefault(require("lodash/debounce"));

var _utils = require("./utils");

var _GridItem = _interopRequireDefault(require("./GridItem"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// End Types
var compactType = function compactType(props
/*: Props*/
)
/*: CompactType*/
{
  var _ref = props || {},
      verticalCompact = _ref.verticalCompact,
      compactType = _ref.compactType;

  return verticalCompact === false ? null : compactType;
};

var layoutClassName = "react-grid-layout";
var isFirefox = false; // Try...catch will protect from navigator not existing (e.g. node) or a bad implementation of navigator

try {
  isFirefox = /firefox/i.test(navigator.userAgent);
} catch (e) {}
/* Ignore */

/**
 * A reactive, fluid grid layout with draggable, resizable components.
 */


var ReactGridLayout =
/*#__PURE__*/
function (_React$Component) {
  _inherits(ReactGridLayout, _React$Component);

  // TODO publish internal ReactClass displayName transform
  function ReactGridLayout(props
  /*: Props*/
  , context
  /*: any*/
  )
  /*: void*/
  {
    var _this;

    _classCallCheck(this, ReactGridLayout);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(ReactGridLayout).call(this, props, context));

    _defineProperty(_assertThisInitialized(_this), "state", {
      activeDrag: null,
      layout: (0, _utils.synchronizeLayoutWithChildren)(_this.props.layout, _this.props.children, _this.props.cols, // Legacy support for verticalCompact: false
      compactType(_this.props)),
      mounted: false,
      oldDragItem: null,
      oldLayout: null,
      oldResizeItem: null,
      droppingDOMNode: null,
      dragSuggestion: false,
      children: []
    });

    _defineProperty(_assertThisInitialized(_this), "dragEnterCounter", 0);

    _defineProperty(_assertThisInitialized(_this), "checkOffPage", function (collisionDetector, x, y) {
      var _this$props = _this.props,
          maxRows = _this$props.maxRows,
          cols = _this$props.cols;

      var cappedCD = _objectSpread({}, collisionDetector);

      var offPage = false; // Left edge collision

      if (x < 0) {
        // Checks that we never go below 1 w/h
        var onPageX = x + cappedCD.w < 1 ? -1 * cappedCD.w + 1 : x;
        cappedCD.w = cappedCD.w + onPageX;
        cappedCD.x = 0;
        offPage = true;
      } // Right edge collision


      if (x + cappedCD.w > cols) {
        // Checks that we never go below 1 w/h
        var _onPageX = x > cols - 1 ? cols - 1 : x;

        cappedCD.w = cappedCD.w - cappedCD.w - _onPageX + cols;
        cappedCD.x = _onPageX;
        offPage = true;
      } // Top Edge collision


      if (y < 0) {
        // Checks that we never go below 1 w/h
        var onPageY = y + cappedCD.h < 1 ? -1 * cappedCD.h + 1 : y;
        cappedCD.h = cappedCD.h + onPageY;
        cappedCD.y = 0;
        offPage = true;
      } // Bottom edge collision


      if (y + cappedCD.h > maxRows) {
        // Checks that we never go below 1 w/h
        var _onPageY = y > maxRows - 1 ? maxRows - 1 : y;

        cappedCD.h = cappedCD.h - cappedCD.h - _onPageY + maxRows;
        cappedCD.y = _onPageY;
        offPage = true;
      }

      return {
        offPage: offPage,
        cappedCD: cappedCD
      };
    });

    _defineProperty(_assertThisInitialized(_this), "shrinkToFit", function (layout, collisionDetector) {
      var maxAreaSpaceToMove = (0, _utils.findLargestAvailableRectangle)(layout, collisionDetector);
      maxAreaSpaceToMove.i = collisionDetector.i;

      if (maxAreaSpaceToMove.h > 0 && maxAreaSpaceToMove.w > 0) {
        _this.setState({
          activeDrag: maxAreaSpaceToMove,
          dragSuggestion: true
        });
      }
    });

    _defineProperty(_assertThisInitialized(_this), "onDragOver", function (e
    /*: DragOverEvent*/
    ) {
      var _this$props2 = _this.props,
          droppingItem = _this$props2.droppingItem,
          cols = _this$props2.cols,
          maxRows = _this$props2.maxRows,
          rowHeight = _this$props2.rowHeight,
          margin = _this$props2.margin,
          containerPadding = _this$props2.containerPadding,
          width = _this$props2.width;

      if (e.target.classList && e.target.classList.contains("QSNotDraggable")) {
        return;
      }

      var layout = _this.state.layout;
      var pageX = e.pageX,
          pageY = e.pageY;
      var colWidth = (0, _utils.calcColWidth)(margin, containerPadding || margin, width, cols); // This means it's off page

      if (pageX === 0 && pageY === 0) return;
      if (!_this.gridSize.current) return; // Set the mouse to the center of the dragged widget

      var rect = _this.gridSize.current.getBoundingClientRect();

      var xOffset = colWidth * _this.props.currentDefaultW / 2;
      var yOffset = rowHeight * _this.props.currentDefaultH / 2;
      var rectX = rect.x ? rect.x : rect.left;
      var rectY = rect.y ? rect.y : rect.top;
      var droppingPosition = {
        x: pageX - rectX - xOffset,
        y: pageY - rectY - yOffset,
        e: e
      };

      if (!_this.state.droppingDOMNode) {
        _this.props.onDragOver(); // PATCH NOTE: This actually adds the item to the layout
        // Before it only showed a placeholder but didn't render the item
        // We set x, y to the bottom right corner to add item as a
        // offgrid hidden element until onDrop fires


        _this.setState({
          droppingDOMNode: _react.default.createElement("div", {
            key: droppingItem.i
          }),
          droppingPosition: droppingPosition,
          layout: [].concat(_toConsumableArray(layout), [{
            i: droppingItem.i,
            w: _this.props.currentDefaultW,
            h: _this.props.currentDefaultH,
            x: cols,
            y: maxRows,
            static: false,
            isDraggable: true
          }])
        });
      } else if (_this.state.droppingPosition) {
        _this.debouncedOnDragOver(droppingPosition, _assertThisInitialized(_this));
      }

      e.stopPropagation();
      e.preventDefault();
    });

    _defineProperty(_assertThisInitialized(_this), "debouncedOnDragOver", (0, _debounce.default)(function (droppingPosition, that) {
      var shouldUpdatePosition = !droppingPosition || !that.state.droppingPosition || that.state.droppingPosition.x != droppingPosition.x || that.state.droppingPosition.y != droppingPosition.y;
      shouldUpdatePosition && that.setState({
        droppingPosition: droppingPosition
      });
    }, 7));

    _defineProperty(_assertThisInitialized(_this), "offscreenRemovePlaceholder", function () {
      var _this$state = _this.state,
          activeDrag = _this$state.activeDrag,
          droppingDOMNode = _this$state.droppingDOMNode,
          droppingPosition = _this$state.droppingPosition,
          dragEnterCounter = _this$state.dragEnterCounter; // This covers the case where the user has dragged off the page
      // But still has a placeholder on the page. It saves the spot
      // And if the user lets go of the mouse will drop in the last location

      if (activeDrag && !dragEnterCounter) {
        _this.onDrop();
      } else if (activeDrag || droppingDOMNode || droppingPosition) {
        // If we have anything related to a placeholder, remove it.
        // This most likely means we missed a cursor event
        _this.setState({
          droppingDOMNode: null,
          activeDrag: null,
          droppingPosition: undefined
        });
      }
    });

    _defineProperty(_assertThisInitialized(_this), "removeDroppingPlaceholder", function () {
      var _this$state2 = _this.state,
          layout = _this$state2.layout,
          activeDrag = _this$state2.activeDrag; // PATCH NOTE: this creates a real grid item in the layout and sets its position

      var layoutNoPlaceholder = layout.map(function (l) {
        if (l.i === "__dropping-elem__") {
          var newL = _objectSpread({}, l, {
            x: activeDrag.x,
            y: activeDrag.y,
            h: activeDrag.h,
            w: activeDrag.w,
            placeholder: false
          });

          return newL;
        }

        return l;
      });

      _this.setState({
        layout: layoutNoPlaceholder,
        droppingDOMNode: null,
        activeDrag: null,
        droppingPosition: undefined
      });

      return layoutNoPlaceholder.i;
    });

    _defineProperty(_assertThisInitialized(_this), "onDragLeave", function () {
      _this.dragEnterCounter--; // Drag listener

      window.addEventListener("drag", _this.onDragOver, false);
    });

    _defineProperty(_assertThisInitialized(_this), "onDragEnter", function () {
      _this.dragEnterCounter++;
      window.removeEventListener("drag", _this.onDragOver, false);
    });

    _defineProperty(_assertThisInitialized(_this), "onDrop", function (e
    /*: Event*/
    ) {
      var activeDrag = _this.state.activeDrag;
      if (!activeDrag) return;
      var x = activeDrag.x,
          y = activeDrag.y,
          w = activeDrag.w,
          h = activeDrag.h; // reset dragEnter counter on drop

      _this.dragEnterCounter = 0; // PATCH NOTE: Added to actually insert the item, remove the activeDrag placeholder

      _this.removeDroppingPlaceholder();

      _this.props.onDrop({
        x: x,
        y: y,
        w: w,
        h: h,
        e: e
      });
    });

    (0, _utils.autoBindHandlers)(_assertThisInitialized(_this), ["onDragStart", "onDrag", "onDragStop", "onResizeStart", "onResize", "onResizeStop", "onDragLeave", "onDragOver"]);
    return _this;
  }

  _createClass(ReactGridLayout, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.setState({
        mounted: true
      });
      this.gridSize = _react.default.createRef();
      window.addEventListener("dragend", this.offscreenRemovePlaceholder, false); // Possibly call back with layout on mount. This should be done after correcting the layout width
      // to ensure we don't rerender with the wrong width.

      this.props.onLayoutChange(this.state.layout);
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      // Remove on component unmount
      window.removeEventListener("dragend", this.offscreenRemovePlaceholder, false);
    }
  }, {
    key: "containerHeight",

    /**
     * Calculates a pixel value for the container.
     * @return {String} Container height in pixels.
     */
    value: function containerHeight() {
      // PATCH NOTE: Only place the fixedNxN is used. Otherwise we can't have maxheight
      if (this.props.fixedNxN) return "100%";
      if (!this.props.autoSize) return;
      var nbRow = (0, _utils.bottom)(this.state.layout);
      var containerPaddingY = this.props.containerPadding ? this.props.containerPadding[1] : this.props.margin[1];
      return nbRow * this.props.rowHeight + (nbRow - 1) * this.props.margin[1] + containerPaddingY * 2 + "px";
    }
    /**
     * When dragging starts
     * @param {String} i Id of the child
     * @param {Number} x X position of the move
     * @param {Number} y Y position of the move
     * @param {Event} e The mousedown event
     * @param {Element} node The current dragging DOM element
     */

  }, {
    key: "onDragStart",
    value: function onDragStart(i
    /*: string*/
    , x
    /*: number*/
    , y
    /*: number*/
    , _ref2) {
      var e = _ref2.e,
          node = _ref2.node;
      var layout = this.state.layout;
      var l = (0, _utils.getLayoutItem)(layout, i);
      if (!l) return;
      this.setState({
        oldDragItem: (0, _utils.cloneLayoutItem)(l),
        oldLayout: this.state.layout
      });
      return this.props.onDragStart(layout, l, x, y, e, node);
    }
    /**
     * Each drag movement create a new dragelement and move the element to the dragged location
     * @param {String} i Id of the child
     * @param {Number} x X position of the move
     * @param {Number} y Y position of the move
     * @param {Event} e The mousedown event
     * @param {Element} node The current dragging DOM element
     */

  }, {
    key: "onDrag",
    value: function onDrag(i
    /*: string*/
    , x
    /*: number*/
    , y
    /*: number*/
    , _ref3) {
      var e = _ref3.e,
          node = _ref3.node;
      console;
      var _this$state3 = this.state,
          layout = _this$state3.layout,
          oldDragItem = _this$state3.oldDragItem;
      var l = (0, _utils.getLayoutItem)(layout, i);
      var cols = this.props.cols;
      if (!l) return; // Create placeholder (display only)

      var placeholder = {
        w: l.w,
        h: l.h,
        x: l.x,
        y: l.y,
        placeholder: true,
        i: i
      }; // PATCH NOTE: USES TOP/LEFT MOST X/Y OF DRAGGED GRID ITEM INSTEAD OF EXISTING LAYOUT ITEM

      var collisionDetector = {
        w: l.w,
        h: l.h,
        x: x,
        y: y,
        placeholder: true,
        i: i
      };

      if (this.props.isShrinkable) {
        // checkOffPage Mutates the collision detector to makes sure we never go off page
        var _this$checkOffPage = this.checkOffPage(collisionDetector, x, y),
            offPage = _this$checkOffPage.offPage,
            cappedCD = _this$checkOffPage.cappedCD;

        var hasCollision = (0, _utils.getFirstCollision)(layout, cappedCD);

        if (hasCollision && this.props.isShrinkable) {
          this.shrinkToFit(layout, cappedCD);
        } else if (offPage) {
          this.setState({
            dragSuggestion: true
          });
          this.props.onDrag(layout, oldDragItem, l, placeholder, e, node);
          this.setState({
            activeDrag: cappedCD
          });
        } else {
          this.setState({
            dragSuggestion: false
          });
          this.props.onDrag(layout, oldDragItem, l, placeholder, e, node);
          this.setState({
            activeDrag: cappedCD
          });
        }
      } else {
        // Move the element to the dragged location.
        var isUserAction = true;
        var newLayout = (0, _utils.moveElement)(layout, l, x, y, isUserAction, this.props.preventCollision, compactType(this.props), cols);
        this.props.onDrag(newLayout, oldDragItem, l, placeholder, e, node);
        this.setState({
          layout: (0, _utils.compact)(newLayout, compactType(this.props), cols),
          activeDrag: placeholder
        });
      }
    }
  }, {
    key: "onDragStop",

    /**
     * When dragging stops, figure out which position the element is closest to and update its x and y.
     * @param  {String} i Index of the child.
     * @param {Number} x X position of the move
     * @param {Number} y Y position of the move
     * @param {Event} e The mousedown event
     * @param {Element} node The current dragging DOM element
     */
    value: function onDragStop(i
    /*: string*/
    , x
    /*: number*/
    , y
    /*: number*/
    , _ref4) {
      var e = _ref4.e,
          node = _ref4.node;
      if (!this.state.activeDrag) return;
      var _this$state4 = this.state,
          oldDragItem = _this$state4.oldDragItem,
          activeDrag = _this$state4.activeDrag;
      var layout = this.state.layout;
      var l;

      var freshLayout = _toConsumableArray(layout); // PATCH NOTE: set dims to placeholder (activeDrag) instead of original size


      freshLayout = layout.map(function (item) {
        if (item.i === activeDrag.i) {
          var newItem = _objectSpread({}, item, {
            w: activeDrag.w,
            h: activeDrag.h,
            x: activeDrag.x,
            y: activeDrag.y,
            placeholder: false
          });

          return newItem;
        }

        return item;
      }); // Set state

      this.props.onDragStop(freshLayout, oldDragItem, l, null, e, node);
      this.setState({
        activeDrag: null,
        layout: freshLayout,
        oldDragItem: null,
        oldLayout: null
      });
      this.props.onLayoutChange(freshLayout);
    }
  }, {
    key: "onResizeStart",
    value: function onResizeStart(i
    /*: string*/
    , w
    /*: number*/
    , h
    /*: number*/
    , _ref5) {
      var e = _ref5.e,
          node = _ref5.node;
      var layout = this.state.layout;
      var l = (0, _utils.getLayoutItem)(layout, i);
      if (!l) return;
      this.setState({
        oldResizeItem: (0, _utils.cloneLayoutItem)(l),
        oldLayout: this.state.layout
      });
      this.props.onResizeStart(layout, l, l, null, e, node);
    }
  }, {
    key: "onResize",
    value: function onResize(i
    /*: string*/
    , w
    /*: number*/
    , h
    /*: number*/
    , _ref6) {
      var e = _ref6.e,
          node = _ref6.node;
      var _this$state5 = this.state,
          layout = _this$state5.layout,
          oldResizeItem = _this$state5.oldResizeItem;
      var l
      /*: ?LayoutItem*/
      = (0, _utils.getLayoutItem)(layout, i);
      if (!l) return;
      var hasCollision = (0, _utils.getFirstCollision)(layout, _objectSpread({}, l, {
        w: w,
        h: h
      }));
      var placeholder; // If we're colliding, we need adjust the placeholder.

      if (hasCollision && this.props.isShrinkable) {
        this.shrinkToFit(layout, _objectSpread({}, l, {
          w: w,
          h: h
        }));
      } else {
        placeholder = {
          w: w,
          h: h,
          x: l.x,
          y: l.y,
          static: true,
          i: i
        };
        this.props.onResize(layout, oldResizeItem, l, placeholder, e, node);
        this.setState({
          activeDrag: placeholder
        });
      }
    }
  }, {
    key: "onResizeStop",
    value: function onResizeStop(i
    /*: string*/
    , w
    /*: number*/
    , h
    /*: number*/
    , _ref7) {
      var e = _ref7.e,
          node = _ref7.node;
      if (!this.state.activeDrag) return;
      var _this$state6 = this.state,
          layout = _this$state6.layout,
          activeDrag = _this$state6.activeDrag;
      var l = (0, _utils.getLayoutItem)(layout, i); // PATCH NOTE: set dims to placeholder instead of original size

      var newLayout = layout.map(function (item) {
        if (item.i === activeDrag.i) {
          var newItem = _objectSpread({}, item, {
            w: activeDrag.w,
            h: activeDrag.h,
            x: activeDrag.x,
            y: activeDrag.y,
            placeholder: false
          });

          return newItem;
        }

        return item;
      });
      var oldLayout = this.state.oldLayout;
      this.props.onResizeStop(newLayout, oldLayout, l, null, e, node);
      this.setState({
        activeDrag: null,
        layout: newLayout,
        oldResizeItem: null,
        oldLayout: null
      });
      this.props.onLayoutChange(newLayout);
    }
    /**
     * Create a placeholder object.
     * @return {Element} Placeholder div.
     */

  }, {
    key: "placeholder",
    value: function placeholder()
    /*: ?ReactElement<any>*/
    {
      var activeDrag = this.state.activeDrag;
      var _this$props3 = this.props,
          width = _this$props3.width,
          cols = _this$props3.cols,
          margin = _this$props3.margin,
          containerPadding = _this$props3.containerPadding,
          rowHeight = _this$props3.rowHeight,
          maxRows = _this$props3.maxRows,
          useCSSTransforms = _this$props3.useCSSTransforms,
          transformScale = _this$props3.transformScale; // {...this.state.activeDrag} is pretty slow, actually

      return _react.default.createElement(_GridItem.default, {
        w: activeDrag.w,
        h: activeDrag.h,
        x: activeDrag.x,
        y: activeDrag.y,
        i: activeDrag.i,
        className: "react-grid-placeholder",
        containerWidth: width,
        cols: cols,
        margin: margin,
        containerPadding: containerPadding || margin,
        maxRows: maxRows,
        rowHeight: rowHeight,
        isDraggable: false,
        isResizable: false,
        useCSSTransforms: useCSSTransforms,
        transformScale: transformScale
      }, _react.default.createElement("div", null));
    }
    /**
     * Given a grid item, set its style attributes & surround in a <Draggable>.
     * @param  {Element} child React element.
     * @return {Element}       Element wrapped in draggable and properly placed.
     */

  }, {
    key: "processGridItem",
    value: function processGridItem(child
    /*: ReactElement<any>*/
    , isDroppingItem
    /*: boolean*/
    )
    /*: ?ReactElement<any>*/
    {
      if (!child || !child.key) return;
      var l = (0, _utils.getLayoutItem)(this.state.layout, String(child.key));
      if (!l) return null;
      var _this$props4 = this.props,
          width = _this$props4.width,
          cols = _this$props4.cols,
          margin = _this$props4.margin,
          containerPadding = _this$props4.containerPadding,
          maxRows = _this$props4.maxRows,
          rowHeight = _this$props4.rowHeight,
          isDraggable = _this$props4.isDraggable,
          isResizable = _this$props4.isResizable,
          useCSSTransforms = _this$props4.useCSSTransforms,
          transformScale = _this$props4.transformScale,
          draggableCancel = _this$props4.draggableCancel,
          draggableHandle = _this$props4.draggableHandle;
      var _this$state7 = this.state,
          mounted = _this$state7.mounted,
          droppingPosition = _this$state7.droppingPosition,
          layout = _this$state7.layout; // Determine user manipulations possible.
      // If an item is static, it can't be manipulated by default.
      // Any properties defined directly on the grid item will take precedence.

      var draggable = typeof l.isDraggable === "boolean" ? l.isDraggable : !l.static && isDraggable;
      var resizable = typeof l.isResizable === "boolean" ? l.isResizable : !l.static && isResizable;
      return _react.default.createElement(_GridItem.default, {
        containerWidth: width,
        cols: cols,
        margin: margin,
        layout: layout,
        containerPadding: containerPadding || margin,
        maxRows: maxRows,
        rowHeight: rowHeight,
        cancel: draggableCancel,
        handle: draggableHandle,
        onDragStop: this.onDragStop,
        onDragStart: this.onDragStart,
        onDrag: this.onDrag,
        onResizeStart: this.onResizeStart,
        onResize: this.onResize,
        onResizeStop: this.onResizeStop,
        isDraggable: draggable,
        isResizable: resizable,
        useCSSTransforms: useCSSTransforms && mounted,
        usePercentages: !mounted,
        transformScale: transformScale,
        w: l.w,
        h: l.h,
        x: l.x,
        y: l.y,
        i: l.i,
        minH: l.minH,
        minW: l.minW,
        maxH: l.maxH,
        maxW: l.maxW,
        static: l.static,
        droppingPosition: isDroppingItem ? droppingPosition : undefined
      }, child);
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var _this$props5 = this.props,
          className = _this$props5.className,
          style = _this$props5.style,
          isDroppable = _this$props5.isDroppable;
      var mergedClassName = (0, _classnames.default)(layoutClassName, className);

      var mergedStyle = _objectSpread({
        height: this.containerHeight()
      }, style);

      return _react.default.createElement("div", {
        ref: this.gridSize,
        className: mergedClassName,
        style: mergedStyle,
        onDrop: isDroppable ? this.onDrop : _utils.noop,
        onDragLeave: isDroppable ? this.onDragLeave : _utils.noop,
        onDragEnter: isDroppable ? this.onDragEnter : _utils.noop,
        onDragOver: isDroppable ? this.onDragOver : _utils.noop
      }, _react.default.Children.map(this.props.children, function (child) {
        return _this2.processGridItem(child);
      }), isDroppable && this.state.droppingDOMNode && this.processGridItem(this.state.droppingDOMNode, true), this.state.activeDrag && this.placeholder());
    }
  }], [{
    key: "getDerivedStateFromProps",
    value: function getDerivedStateFromProps(nextProps
    /*: Props*/
    , prevState
    /*: State*/
    ) {
      var newLayoutBase;

      if (prevState.activeDrag) {
        return null;
      } // Legacy support for compactType
      // Allow parent to set layout directly.


      if (!(0, _lodash.default)(nextProps.layout, prevState.propsLayout) || nextProps.compactType !== prevState.compactType) {
        newLayoutBase = nextProps.layout;
      } else if (!(0, _utils.childrenEqual)(nextProps.children, prevState.children)) {
        // If children change, also regenerate the layout. Use our state
        // as the base in case because it may be more up to date than
        // what is in props.
        newLayoutBase = prevState.layout;
      } // We need to regenerate the layout.


      if (newLayoutBase) {
        var newLayout = (0, _utils.synchronizeLayoutWithChildren)(newLayoutBase, nextProps.children, nextProps.cols, compactType(nextProps));
        return {
          layout: newLayout,
          // We need to save these props to state for using
          // getDerivedStateFromProps instead of componentDidMount (in which we would get extra rerender)
          compactType: nextProps.compactType,
          children: nextProps.children,
          propsLayout: nextProps.layout
        };
      }

      return null;
    }
  }]);

  return ReactGridLayout;
}(_react.default.Component);

exports.default = ReactGridLayout;

_defineProperty(ReactGridLayout, "displayName", "ReactGridLayout");

_defineProperty(ReactGridLayout, "propTypes", {
  //
  // Basic props
  //
  className: _propTypes.default.string,
  style: _propTypes.default.object,
  // This can be set explicitly. If it is not set, it will automatically
  // be set to the container width. Note that resizes will *not* cause this to adjust.
  // If you need that behavior, use WidthProvider.
  width: _propTypes.default.number,
  // If true, the container height swells and contracts to fit contents
  autoSize: _propTypes.default.bool,
  // # of cols.
  cols: _propTypes.default.number,
  // A selector that will not be draggable.
  draggableCancel: _propTypes.default.string,
  // A selector for the draggable handler
  draggableHandle: _propTypes.default.string,
  // Deprecated
  verticalCompact: function verticalCompact(props
  /*: Props*/
  ) {
    if (props.verticalCompact === false && process.env.NODE_ENV !== "production") {
      console.warn( // eslint-disable-line no-console
      "`verticalCompact` on <ReactGridLayout> is deprecated and will be removed soon. " + 'Use `compactType`: "horizontal" | "vertical" | null.');
    }
  },
  // Choose vertical or hotizontal compaction
  compactType: _propTypes.default.oneOf(["vertical", "horizontal"]),
  // layout is an array of object with the format:
  // {x: Number, y: Number, w: Number, h: Number, i: String}
  layout: function layout(props
  /*: Props*/
  ) {
    var layout = props.layout; // I hope you're setting the data-grid property on the grid items

    if (layout === undefined) return;
    (0, _utils.validateLayout)(layout, "layout");
  },
  //
  // Grid Dimensions
  //
  // Margin between items [x, y] in px
  margin: _propTypes.default.arrayOf(_propTypes.default.number),
  // Padding inside the container [x, y] in px
  containerPadding: _propTypes.default.arrayOf(_propTypes.default.number),
  // Rows have a static height, but you can change this based on breakpoints if you like
  rowHeight: _propTypes.default.number,
  // Default Infinity, but you can specify a max here if you like.
  // Note that this isn't fully fleshed out and won't error if you specify a layout that
  // extends beyond the row capacity. It will, however, not allow users to drag/resize
  // an item past the barrier. They can push items beyond the barrier, though.
  // Intentionally not documented for this reason.
  maxRows: _propTypes.default.number,
  //
  // Flags
  //
  isDraggable: _propTypes.default.bool,
  isResizable: _propTypes.default.bool,
  // If true, grid items won't change position when being dragged over.
  preventCollision: _propTypes.default.bool,
  // Use CSS transforms instead of top/left
  useCSSTransforms: _propTypes.default.bool,
  // parent layout transform scale
  transformScale: _propTypes.default.number,
  // If true, an external element can trigger onDrop callback with a specific grid position as a parameter
  isDroppable: _propTypes.default.bool,
  fixedNxN: _propTypes.default.bool,
  //
  // Callbacks
  //
  // Callback so you can save the layout. Calls after each drag & resize stops.
  onLayoutChange: _propTypes.default.func,
  // Calls when drag starts. Callback is of the signature (layout, oldItem, newItem, placeholder, e, ?node).
  // All callbacks below have the same signature. 'start' and 'stop' callbacks omit the 'placeholder'.
  onDragStart: _propTypes.default.func,
  // Calls on each drag movement.
  onDrag: _propTypes.default.func,
  // Calls when dragged into movement area
  onDragOver: _propTypes.default.func,
  // Calls when drag is complete.
  onDragStop: _propTypes.default.func,
  //Calls when resize starts.
  onResizeStart: _propTypes.default.func,
  // Calls when resize movement happens.
  onResize: _propTypes.default.func,
  // Calls when resize is complete.
  onResizeStop: _propTypes.default.func,
  // Calls when some element is dropped.
  onDrop: _propTypes.default.func,
  //
  // Other validations
  //
  droppingItem: _propTypes.default.shape({
    i: _propTypes.default.string.isRequired,
    w: _propTypes.default.number.isRequired,
    h: _propTypes.default.number.isRequired
  }),
  // Children must not have duplicate keys.
  children: function children(props
  /*: Props*/
  , propName
  /*: string*/
  ) {
    var children = props[propName]; // Check children keys for duplicates. Throw if found.

    var keys = {};

    _react.default.Children.forEach(children, function (child) {
      if (keys[child.key]) {
        throw new Error('Duplicate child key "' + child.key + '" found! This will cause problems in ReactGridLayout.');
      }

      keys[child.key] = true;
    });
  },
  //flag to turn on and off the feature that resizes a GridItem when a collision occurs
  isShrinkable: _propTypes.default.bool
});

_defineProperty(ReactGridLayout, "defaultProps", {
  autoSize: true,
  cols: 12,
  className: "",
  style: {},
  draggableHandle: "",
  draggableCancel: "",
  containerPadding: null,
  rowHeight: 150,
  maxRows: Infinity,
  // infinite vertical growth
  layout: [],
  margin: [10, 10],
  isDraggable: true,
  isResizable: true,
  isDroppable: false,
  useCSSTransforms: true,
  transformScale: 1,
  verticalCompact: true,
  compactType: "vertical",
  preventCollision: false,
  droppingItem: {
    i: "__dropping-elem__",
    w: 1,
    h: 1
  },
  fixedNxN: false,
  currentDefaultW: 1,
  currentDefaultH: 1,
  isShrinkable: true,
  onLayoutChange: _utils.noop,
  onDragStart: _utils.noop,
  onDragOver: _utils.noop,
  onDrag: _utils.noop,
  onDragStop: _utils.noop,
  onResizeStart: _utils.noop,
  onResize: _utils.noop,
  onResizeStop: _utils.noop,
  onDrop: _utils.noop
});